const { Engine, Render, Runner, World, Bodies, Body, Events } = Matter;

const cellsHorizontal = 10;
const cellsVertical = 9;
const width = window.innerWidth;
const height = window.innerHeight;
const widthMiddle = width / 2;
const heightMiddle = height / 2;
const unitLengthX = width / cellsHorizontal;
const unitLengthY = height / cellsVertical;

if (/Android|webOS|iPhone|iPad|iPod/i.test(navigator.userAgent)) {
	document.querySelector('.game').classList.add('.hidden');
	document.querySelector('.mobile-message').style = 'display: flex';
}

const engine = Engine.create();
engine.world.gravity.y = 0;
const { world } = engine;
const render = Render.create({
	element: document.querySelector('.game'),
	engine,
	options: {
		wireframes: false,
		width,
		height,
	},
});
Render.run(render);
Runner.run(Runner.create(), engine);

// walls
const walls = [
	Bodies.rectangle(widthMiddle, 0, width, 2, { isStatic: true }),
	Bodies.rectangle(widthMiddle, height, width, 2, { isStatic: true }),
	Bodies.rectangle(0, heightMiddle, 2, height, { isStatic: true }),
	Bodies.rectangle(width, heightMiddle, 2, height, { isStatic: true }),
];

World.add(world, walls);

// maze generation
const shuffle = (arr) => {
	let counter = arr.length;

	while (counter > 0) {
		const index = Math.floor(Math.random() * counter);
		counter--;
		const temp = arr[counter];
		arr[counter] = arr[index];
		arr[index] = temp;
	}
	return arr;
};

const grid = Array(cellsVertical)
	.fill(null)
	.map(() => Array(cellsHorizontal).fill(false));

// verticals generation
const verticals = Array(cellsVertical)
	.fill(null)
	.map(() => Array(cellsHorizontal - 1).fill(false));
// horizontals generation
const horizontals = Array(cellsVertical - 1)
	.fill(null)
	.map(() => Array(cellsHorizontal).fill(false));

const startRow = Math.floor(Math.random() * cellsVertical);
const startColumn = Math.floor(Math.random() * cellsHorizontal);

const stepThroughCell = (row, column) => {
	// if cell was visited return
	if (grid[row][column]) return;

	// mark cell as visited
	grid[row][column] = true;

	// assemble randomly-ordered list of neighbors
	const neighbors = shuffle([
		[row - 1, column, 'up'], //
		[row, column + 1, 'right'],
		[row + 1, column, 'down'],
		[row, column - 1, 'left'],
	]);

	// for each neighbor...
	for (const neighbor of neighbors) {
		const [nextRow, nextColumn, direction] = neighbor;
		// see if that neighbor is out of bounds
		if (
			nextRow < 0 ||
			nextRow >= cellsVertical ||
			nextColumn < 0 ||
			nextColumn >= cellsHorizontal
		)
			continue;

		// if we have bisted that neighbor, continue to the next neighbor
		if (grid[nextRow][nextColumn]) continue;

		// remove a wall from either verticals or horizontals
		if (direction === 'left') {
			verticals[row][column - 1] = true;
		} else if (direction === 'right') {
			verticals[row][column] = true;
		} else if (direction === 'up') {
			horizontals[row - 1][column] = true;
		} else if (direction === 'down') {
			horizontals[row][column] = true;
		}
		// visit that next cell
		stepThroughCell(nextRow, nextColumn);
	}
};

stepThroughCell(startRow, startColumn);

// draw horizontal lines
horizontals.forEach((row, rowIndex) => {
	row.forEach((open, columnIndex) => {
		if (open) return;
		const xCoord = columnIndex * unitLengthX + unitLengthX / 2;
		const yCoord = rowIndex * unitLengthY + unitLengthY;
		const wall = Bodies.rectangle(xCoord, yCoord, unitLengthX, 5, {
			isStatic: true,
			label: 'wall',
			render: {
				fillStyle: 'red',
			},
		});
		World.add(world, wall);
	});
});

// draw verticals lines
verticals.forEach((row, rowIndex) => {
	row.forEach((open, columnIndex) => {
		if (open) return;
		const xCoord = columnIndex * unitLengthX + unitLengthX;
		const yCoord = rowIndex * unitLengthY + unitLengthY / 2;
		const wall = Bodies.rectangle(xCoord, yCoord, 5, unitLengthY, {
			isStatic: true,
			label: 'wall',
			render: {
				fillStyle: 'red',
			},
		});
		World.add(world, wall);
	});
});

// goal
const goal = Bodies.rectangle(
	width - unitLengthX / 2,
	height - unitLengthY / 2,
	unitLengthX * 0.7,
	unitLengthY * 0.7,
	{
		isStatic: true,
		label: 'goal',
		render: {
			fillStyle: 'green',
		},
	}
);
World.add(world, goal);

// ball
const ballRadius = Math.min(unitLengthX, unitLengthY) / 4;
const ball = Bodies.circle(unitLengthX / 2, unitLengthY / 2, ballRadius, {
	label: 'ball',
	render: {
		fillStyle: 'blue',
	},
});
World.add(world, ball);

// move ball
document.addEventListener('keydown', (e) => {
	const { x, y } = ball.velocity;
	if (e.key === 'ArrowUp') Body.setVelocity(ball, { x, y: y - 5 });
	if (e.key === 'ArrowLeft') Body.setVelocity(ball, { x: x - 5, y });
	if (e.key === 'ArrowDown') Body.setVelocity(ball, { x, y: y + 5 });
	if (e.key === 'ArrowRight') Body.setVelocity(ball, { x: x + 5, y });
});

// win condition
Events.on(engine, 'collisionStart', (event) => {
	event.pairs.forEach((collision) => {
		const labels = ['ball', 'goal'];
		if (
			labels.includes(collision.bodyA.label) &&
			labels.includes(collision.bodyB.label)
		) {
			document.querySelector('.winner').classList.remove('hidden');
			world.gravity.y = 1;
			world.bodies.forEach((body) => {
				if (body.label === 'wall') Body.setStatic(body, false);
			});
		}
	});
});
